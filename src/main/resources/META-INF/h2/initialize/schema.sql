DROP TABLE hoge if exists;

CREATE TABLE hoge (
  id   VARCHAR(5)  PRIMARY KEY,
  name VARCHAR(20)
);

INSERT INTO hoge VALUES('1','poyo');
INSERT INTO hoge VALUES('2','fuga');