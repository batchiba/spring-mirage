package jp.co.lty.config;

import jp.sf.amateras.mirage.SqlManagerImpl;
import jp.sf.amateras.mirage.dialect.Dialect;
import jp.sf.amateras.mirage.dialect.H2Dialect;
import jp.sf.amateras.mirage.integration.spring.SpringConnectionProvider;
import net.sf.log4jdbc.Log4jdbcProxyDataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@EnableTransactionManagement
@Configuration
@ComponentScan({"jp.co.lty"})
public class AppConfig {

    @Bean
    public DataSource dataSource() {
        DataSource dataSource = createDataSource();
        DatabasePopulatorUtils.execute(createDatabasePopulator(), dataSource);
        return dataSource;
    }

    private DataSource createDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mem:test");
        dataSource.setUsername("sa");
        dataSource.setPassword("");
        return new TransactionAwareDataSourceProxy(new Log4jdbcProxyDataSource(dataSource));
    }

    private DatabasePopulator createDatabasePopulator() {
        ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
        databasePopulator.setContinueOnError(true);
        databasePopulator.addScript(new ClassPathResource("/META-INF/h2/initialize/schema.sql"));
        return databasePopulator;
    }

    @Bean
    public Dialect dialect() {
        return new H2Dialect();
    }

    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(this.dataSource());
    }

    @Bean
    public SpringConnectionProvider connectionProvider() {
        SpringConnectionProvider provider = new SpringConnectionProvider();
        provider.setTransactionManager(this.transactionManager());
        provider.setDataSource(this.dataSource());
        return provider;
    }

    @Bean
    public SqlManagerImpl sqlManager() {
        SqlManagerImpl sqlManager = new SqlManagerImpl();
        sqlManager.setConnectionProvider(this.connectionProvider());
        sqlManager.setDialect(this.dialect());
        return sqlManager;
    }
}