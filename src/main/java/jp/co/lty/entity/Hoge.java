package jp.co.lty.entity;

import jp.sf.amateras.mirage.annotation.PrimaryKey;
import jp.sf.amateras.mirage.annotation.PrimaryKey.GenerationType;
import jp.sf.amateras.mirage.annotation.Table;

@Table(name = "hoge")
public class Hoge {

    @PrimaryKey(generationType = GenerationType.APPLICATION)
    public String id;
    public String name;
}