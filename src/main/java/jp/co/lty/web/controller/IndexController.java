package jp.co.lty.web.controller;

import jp.co.lty.web.service.IndexService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

	private final Logger logger = LoggerFactory.getLogger(IndexController.class);

	@Autowired
	private IndexService indexService;

	@RequestMapping(value = "/")
	public String doMirage(Model model) {
        indexService.execute();
        model.addAttribute("title", "Hello World！");
        model.addAttribute("msg", "Here is Spring-Mirage example page! ゆっくりしていってね！ なんもないけど");
        model.addAttribute("msg2", "バカめ！　すでに初期表示時にmirage連携してるわ！！ m9(^Д^)ﾌﾟｷﾞｬｰ");
        return "index";
	}
}