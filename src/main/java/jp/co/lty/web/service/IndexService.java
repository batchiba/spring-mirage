package jp.co.lty.web.service;

import jp.co.lty.entity.Hoge;
import jp.sf.amateras.mirage.ClasspathSqlResource;
import jp.sf.amateras.mirage.SqlManager;
import jp.sf.amateras.mirage.SqlResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class IndexService {

	@Autowired
	private SqlManager sqlManager;

	private static final Logger logger = LoggerFactory.getLogger(IndexService.class);

    @Transactional
	public void execute() {
        SqlResource selectHogeAllSql = new ClasspathSqlResource("META-INF/mirage/hoge/selectAll.sql");
        sqlManager.getResultList(Hoge.class, selectHogeAllSql).forEach(
                hoge -> logger.info("hoge.id [" + hoge.id + "] hoge.name[" + hoge.name + "]"));
	}
}