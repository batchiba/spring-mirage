<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>Spring-Mirage Project</title>
	<meta charset="UTF-8">
	<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
	<link href="${bootstrapCss}" rel="stylesheet" />
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Spring-Mirage Project</a>
            </div>
        </div>
    </nav>
    <div class="jumbotron">
        <div class="container">
            <h1>${title}</h1><br>
            <p>${msg}</p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-dialog" onclick="alert('${msg2}');">doMirage</button>
                <hr>
            </div>
        </div>
        <footer><p>Copywirte &copy; 2015 Lty All Rights Reserved.</p></footer>
    </div>
</body>
</html>